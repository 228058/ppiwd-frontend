/*
 * Copyright 2015 MbientLab Inc. All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights
 * granted under the terms of a software license agreement between the user who
 * downloaded the software, his/her employer (which must be your employer) and
 * MbientLab Inc, (the "License").  You may not use this Software unless you
 * agree to abide by the terms of the License which can be found at
 * www.mbientlab.com/terms . The License limits your use, and you acknowledge,
 * that the  Software may not be modified, copied or distributed and can be used
 * solely and exclusively in conjunction with a MbientLab Inc, product.  Other
 * than for the foregoing purpose, you may not use, reproduce, copy, prepare
 * derivative works of, modify, distribute, perform, display or sell this
 * Software and/or its documentation for any purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
 * PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
 * MBIENTLAB OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE,
 * STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 * THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED
 * TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST
 * PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
 * SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY
 * DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software,
 * contact MbientLab Inc, at www.mbientlab.com.
 */

package com.mbientlab.metawear.tutorial.starter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.data.Acceleration;
import com.mbientlab.metawear.data.AngularVelocity;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.GyroBmi160;
import com.mbientlab.metawear.tutorial.starter.DeviceSetupActivityFragment.FragmentSettings;

import bolts.Task;

import static android.content.DialogInterface.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.CancellationException;


public class DeviceSetupActivity extends AppCompatActivity implements ServiceConnection, FragmentSettings {
    public final static String EXTRA_BT_DEVICE= "com.mbientlab.metawear.starter.DeviceSetupActivity.EXTRA_BT_DEVICE";

    public static class ReconnectDialogFragment extends DialogFragment implements  ServiceConnection {
        private static final String KEY_BLUETOOTH_DEVICE = "com.mbientlab.metawear.starter.DeviceSetupActivity.ReconnectDialogFragment.KEY_BLUETOOTH_DEVICE";

        private ProgressDialog reconnectDialog = null;
        private BluetoothDevice btDevice = null;
        private MetaWearBoard currentMwBoard = null;

        public static ReconnectDialogFragment newInstance(BluetoothDevice btDevice) {
            Bundle args = new Bundle();
            args.putParcelable(KEY_BLUETOOTH_DEVICE, btDevice);

            ReconnectDialogFragment newFragment = new ReconnectDialogFragment();
            newFragment.setArguments(args);

            return newFragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            btDevice = getArguments().getParcelable(KEY_BLUETOOTH_DEVICE);
            getActivity().getApplicationContext().bindService(new Intent(getActivity(), BtleService.class), this, BIND_AUTO_CREATE);

            reconnectDialog = new ProgressDialog(getActivity());
            reconnectDialog.setTitle(getString(R.string.title_reconnect_attempt));
            reconnectDialog.setMessage(getString(R.string.message_wait));
            reconnectDialog.setCancelable(false);
            reconnectDialog.setCanceledOnTouchOutside(false);
            reconnectDialog.setIndeterminate(true);
            reconnectDialog.setButton(BUTTON_NEGATIVE, getString(android.R.string.cancel), (dialogInterface, i) -> {
                currentMwBoard.disconnectAsync();
                getActivity().finish();
            });

            return reconnectDialog;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            currentMwBoard= ((BtleService.LocalBinder) service).getMetaWearBoard(btDevice);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) { }
    }

    private BluetoothDevice btDevice;
    private MetaWearBoard metawear;

    private Accelerometer accelerometer;
    private GyroBmi160 gyro;

    private final String RECONNECT_DIALOG_TAG= "reconnect_dialog_tag";

    private boolean isSitting = false;
    private boolean recordingData = false;
    private boolean connectionWasRunning = false;

    private Button startButton;
    private Button stopButton;
    private Switch sittingSwitch;

    double acc_raw_x;
    double acc_raw_y;
    double acc_raw_z;
    double gyro_raw_x;
    double gyro_raw_y;
    double gyro_raw_z;

    String doubleSensorUrlString = "https://ppiwd-server.azurewebsites.net/data/array/";

    private final int N_SAMPLES = 1000;

    JSONObject dataObject;
    JSONArray dataArray;
    private boolean accRead = true;

    long lastTimestamp = 0;

    private void sendData(String endpoint, String jsonString) {
        try {
            URL url = new URL(endpoint);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "application/json; utf-8");
            connection.setRequestProperty("Accept", "application/json");

            // request part
            connection.connect();
            Log.i("starter", "json: " + jsonString);
            PrintStream ps = new PrintStream(connection.getOutputStream());
            ps.print(jsonString);
            ps.close();

            // response part
            if (connection.getResponseCode() != 200) {
                Log.i("starter", "POST error, code: " + Integer.toString(connection.getResponseCode()));
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            String result = sb.toString();
            Log.i("starter", "result: " + result);

            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void accTask() throws Exception {
        Task accTask = accelerometer.acceleration().addRouteAsync(source -> {
            source.stream((data, env) -> {
                if(accRead)
                {
                    acc_raw_x = data.value(Acceleration.class).x();
                    acc_raw_y = data.value(Acceleration.class).y();
                    acc_raw_z = data.value(Acceleration.class).z();
                    accRead = false;
                }
            });
        });
        accTask.waitForCompletion();
        boolean accTaskSuccess = accTask.isCompleted();
        if (accTaskSuccess) {
            Log.i("starter", " acc task completed");
        } else if (accTask.isFaulted()) {
            Log.i("starter", " acc task failed");
            throw accTask.getError();
        } else if (accTask.isCancelled()) {
            Log.i("starter", " acc task cancelled");
            throw new CancellationException("Task cancelled");
        } else {
            Log.i("starter", " acc task not completed");
            throw new Exception("Task cancelled");
        }
    }

    private void gyroTask() throws Exception {
        @SuppressLint("HardwareIds") Task gyroTask = gyro.angularVelocity().addRouteAsync(source -> {
            source.stream((data, env) -> {
                long currentTimestamp = System.currentTimeMillis();
                if(!accRead && !(lastTimestamp/10 == currentTimestamp/10))
                {
                    gyro_raw_x = data.value(AngularVelocity.class).x();
                    gyro_raw_y = data.value(AngularVelocity.class).y();
                    gyro_raw_z = data.value(AngularVelocity.class).z();

                    try {
                        dataObject = new JSONObject();
                        dataObject.put("device_id", Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        dataObject.put("sitting", isSitting ? 1 : 0);
                        dataObject.put("timestamp", currentTimestamp);
                        dataObject.put("acc_x", acc_raw_x);
                        dataObject.put("acc_y", acc_raw_y);
                        dataObject.put("acc_z", acc_raw_z);
                        dataObject.put("gyro_x", gyro_raw_x);
                        dataObject.put("gyro_y", gyro_raw_y);
                        dataObject.put("gyro_z", gyro_raw_z);
                        dataArray.put(dataObject);
                        lastTimestamp = currentTimestamp;
                    } catch (JSONException e) {
                        Log.i("starter", "jsonException exception");
                        e.printStackTrace();
                    }
//                    Log.i("starter", "dataArray length = " + dataArray.length());
                    if(dataArray.length() == N_SAMPLES)
                    {
                        sendData(doubleSensorUrlString, dataArray.toString());
                        dataArray = new JSONArray();
                    }

                    accRead = true;
                }

            });
        });
        gyroTask.waitForCompletion();
        boolean gyroTaskSuccess = gyroTask.isCompleted();
        if (gyroTaskSuccess) {
            Log.i("starter", " gyro task completed");
        } else if (gyroTask.isFaulted()) {
            Log.i("starter", " gyro task failed");
            throw gyroTask.getError();
        } else if (gyroTask.isCancelled()) {
            Log.i("starter", " gyro task cancelled");
            throw new CancellationException("Task cancelled");
        } else {
            Log.i("starter", " gyro task not completed");
            throw new Exception("Task cancelled");
        }
    }

    public void fuseImuData() throws Exception
    {
        accTask();
        gyroTask();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_setup);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dataArray = new JSONArray();

        btDevice= getIntent().getParcelableExtra(EXTRA_BT_DEVICE);
        connectionWasRunning = getIntent().getExtras().getBoolean("connectionWasRunning");
        getApplicationContext().bindService(new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);

        startButton = findViewById(R.id.start);
        stopButton = findViewById(R.id.stop);
        sittingSwitch = findViewById(R.id.sittingSwitch);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recordingData = true;
                MainActivity.recordingData = true;
                Log.i("starter", "MainActivity recordingData value: " + MainActivity.recordingData);
                Log.i("starter", "sitting: " + isSitting);

                gyro.angularVelocity().start();
                gyro.start();

                accelerometer.acceleration().start();
                accelerometer.start();

                startButton.setEnabled(false);
                stopButton.setEnabled(true);

            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recordingData = false;
                MainActivity.recordingData = false;
                Log.i("starter", "MainActivity recordingData value: " + MainActivity.recordingData);
                Log.i("starter", "sitting: " + isSitting);

                accelerometer.acceleration().stop();
                accelerometer.stop();

                gyro.angularVelocity().stop();
                gyro.stop();

                startButton.setEnabled(true);
                stopButton.setEnabled(false);
            }
        });

        sittingSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sittingSwitch.setActivated(!sittingSwitch.isActivated());
                isSitting = sittingSwitch.isActivated();
                MainActivity.sitting = isSitting;
                Log.i("starter", "MainActivity sitting value: " + MainActivity.sitting);
                Log.i("starter", "sitting: " + isSitting);
            }
        });

        if(!connectionWasRunning)
        {
            startButton.setEnabled(true);
            stopButton.setEnabled(false);
            sittingSwitch.setActivated(false);
        }
        else // after unexpected disconnect
        {
            Log.i("starter", "config after unexpected disconnect");
            isSitting = MainActivity.sitting;
            if(isSitting)
            {
                sittingSwitch.setActivated(true);
            }
            else
            {
                sittingSwitch.setActivated(false);
            }

            recordingData = MainActivity.recordingData;
            if(recordingData)
            {
                startButton.setEnabled(false);
                stopButton.setEnabled(true);
            }
            else
            {
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
            }
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_disconnect:
                metawear.disconnectAsync();
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        metawear.disconnectAsync();
        super.onBackPressed();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.i("starter", "service connected");
        metawear = ((BtleService.LocalBinder) service).getMetaWearBoard(btDevice);
        metawear.onUnexpectedDisconnect(status -> {
            ReconnectDialogFragment dialogFragment= ReconnectDialogFragment.newInstance(btDevice);
            dialogFragment.show(getSupportFragmentManager(), RECONNECT_DIALOG_TAG);
        });
        metawear.connectAsync().onSuccessTask(task -> {
            accelerometer = metawear.getModule(Accelerometer.class);
            accelerometer.configure()
                    .odr(100f)      // Set sampling frequency to 100Hz, or closest valid ODR
                    .range(4f)      // Set data range to +/-4g, or closet valid range
                    .commit();
            if(connectionWasRunning)
            {
                accelerometer.acceleration().start();
                accelerometer.start();
            }
            else
            {
                accelerometer.acceleration().stop();
                accelerometer.stop();
            }

            gyro = metawear.getModule(GyroBmi160.class);
            gyro.configure()
                    .odr(GyroBmi160.OutputDataRate.ODR_100_HZ)
                    .range(GyroBmi160.Range.FSR_2000)
                    .commit();
            if(connectionWasRunning)
            {
                gyro.angularVelocity().start();
                gyro.start();
            }
            else
            {
                gyro.angularVelocity().stop();
                gyro.stop();
            }


            return null;
        }).continueWith(task -> {
            if (task.isCompleted()) {
                Log.i("starter", " Connected");
                try {
                    fuseImuData();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
            else if (task.isFaulted()) {
                Log.i("starter", " Failed to connect");
            }
            else if (task.isCancelled()){
                Log.i("starter", " Connection task cancelled");
            }
            return null;
        });
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public BluetoothDevice getBtDevice() {
        return btDevice;
    }
}


package com.example.report;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultListAdapter extends ArrayAdapter<Result> {
    private  Context mContext;
    private int mResource;
    public ResultListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Result> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String mainHeader = getItem(position).getMainHeader();
        String sittingValue = getItem(position).getSittingValue();
        String activityValue = getItem(position).getActivityValue();

        Result result = new Result(mainHeader, sittingValue, activityValue);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView mH = convertView.findViewById(R.id.mainHeader);
        TextView sV = convertView.findViewById(R.id.sittingHeader);
        TextView aV = convertView.findViewById(R.id.activityHeader);

        mH.setText(mainHeader);
        sV.setText(sittingValue);
        aV.setText(activityValue);
        return convertView;
    }
}

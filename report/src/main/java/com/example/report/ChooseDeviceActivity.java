package com.example.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mbientlab.bletoolbox.scanner.BleScannerFragment;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.android.BtleService;

import java.util.UUID;

import bolts.Task;

public class ChooseDeviceActivity extends AppCompatActivity implements BleScannerFragment.ScannerCommunicationBus, ServiceConnection {
    public static final int REQUEST_BT_DEVICE= 1;

    public static boolean sitting = false;
    public static boolean recordingData = false;

    private BtleService.LocalBinder serviceBinder;
    private MetaWearBoard metawear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_device);

        getApplicationContext().bindService(new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        ///< Unbind the service when the activity is destroyed
//        getApplicationContext().unbindService(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQUEST_BT_DEVICE:
                ((BleScannerFragment) getFragmentManager().findFragmentById(R.id.scanner_fragment)).startBleScan();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public UUID[] getFilterServiceUuids() {
        return new UUID[] {MetaWearBoard.METAWEAR_GATT_SERVICE};
    }

    @Override
    public long getScanDuration() {
        return 10000L;
    }

    @Override
    public void onDeviceSelected(final BluetoothDevice device) {
        metawear = serviceBinder.getMetaWearBoard(device);


        final ProgressDialog connectDialog = new ProgressDialog(this);
        connectDialog.setTitle(getString(R.string.title_connecting));
        connectDialog.setMessage(getString(R.string.message_wait));
        connectDialog.setCancelable(false);
        connectDialog.setCanceledOnTouchOutside(false);
        connectDialog.setIndeterminate(true);
        connectDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(android.R.string.cancel), (dialogInterface, i) -> metawear.disconnectAsync());
        connectDialog.show();

        metawear.onUnexpectedDisconnect(new MetaWearBoard.UnexpectedDisconnectHandler() {
            @Override
            public void disconnected(int status) {
                    Log.i("report", "Unexpectedly lost connection: " + status);
                    Log.i("report", "Attempting to reconnect");
                    metawear.connectAsync().continueWithTask(task -> task.isCancelled() || !task.isFaulted() ? task : reconnect(metawear))
                            .continueWith(task -> {
                                if (!task.isCancelled()) {
                                    runOnUiThread(connectDialog::dismiss);
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("result",device);
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }

                                return null;
                            });
            }
        });

        metawear.connectAsync().continueWithTask(task -> task.isCancelled() || !task.isFaulted() ? task : reconnect(metawear))
                .continueWith(task -> {
                    if (!task.isCancelled()) {
                        runOnUiThread(connectDialog::dismiss);
//                        Intent navActivityIntent = new Intent(ChooseDeviceActivity.this, MainActivity.class);
////                        navActivityIntent.putExtra(MainActivity.EXTRA_BT_DEVICE, device);
////                        navActivityIntent.putExtra("connectionWasRunning", false);
                        Intent returnIntent = new Intent(ChooseDeviceActivity.this, MainActivity.class);
                        returnIntent.putExtra("result",device.toString());
                        if(device == null)
                        {
                            Log.i("report", "device is null when returning");
                        }
                        else
                        {
                            Log.i("report", "device is NOT null when returning");
                        }
                        returnIntent.putExtra("device",device);
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
//                        setResult(Activity.RESULT_OK,returnIntent);
                    }

                    return null;
                });

    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        serviceBinder = (BtleService.LocalBinder) service;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    public static Task<Void> reconnect(final MetaWearBoard board) {
        return board.connectAsync().continueWithTask(task -> task.isFaulted() ? reconnect(board) : task);
    }
}


package com.example.report;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Pair;
import java.util.Calendar;

enum Interval{
    Days,
    Weeks,
    Months,
    Years
}

// Returns Unix timestamps
public  class TimeIntervalsHelper {
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static Pair<Long, Long> GetTimestamps(int count, Interval interval){
        Calendar day = Calendar.getInstance();

        // reset time to midnight
        day.set(Calendar.MILLISECOND, 0);
        day.set(Calendar.SECOND, 0);
        day.set(Calendar.MINUTE, 0);
        day.set(Calendar.HOUR_OF_DAY, 0);

        long tEnd = day.getTimeInMillis();
        int mltp;

        switch (interval){
            case Days:
            default:
            {
                mltp = -1;
                break;
            }
            case Weeks:
            {
                mltp = -7;
                break;
            }
        }

        day.add(Calendar.DAY_OF_MONTH, count * mltp);
        long tStart = day.getTimeInMillis();
        return new Pair<>(tStart, tEnd);
    }
}

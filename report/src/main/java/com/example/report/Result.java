package com.example.report;

public class Result {
    private String mainHeader;
    private String sittingValue;
    private String activityValue;

    public Result(String mainHeader, String sittingValue, String activityValue){
        this.mainHeader = mainHeader;
        this.sittingValue = "Sitting - " + sittingValue;
        this.activityValue = "Activity - " + activityValue;
    }

    public String getMainHeader(){
        return this.mainHeader;
    }

    public String getSittingValue(){
        return this.sittingValue;
    }

    public String getActivityValue(){
        return this.activityValue;
    }
}

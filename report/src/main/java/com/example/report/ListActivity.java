package com.example.report;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private String[] items = new String[]{"Weekly", "Monthly", "Yearly"};
    private String selectedItem = "Weekly";
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ImageButton settingsButton = findViewById(R.id.imageButtonSettingsButton);
        settingsButton.setOnClickListener(view -> {
            Intent i = new Intent(this, SettingsActivity.class);
            this.startActivity(i);
        });

        Spinner spinner = findViewById(R.id.spinnerList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ListActivity.this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedItem = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        setLineChartData();

        listView = findViewById(R.id.listView);
        Result r1 = new Result("12.03", "1h 50m", "2h 30m");
        Result r2 = new Result("13.03", "5h 00m", "1h 20m");
        Result r3 = new Result("14.03", "8h 20m", "2h 10m");

        ArrayList<Result> results = new ArrayList<>();
        results.add(r1);
        results.add(r2);
        results.add(r3);
        results.add(r1);
        results.add(r2);
        results.add(r3);results.add(r1);
        results.add(r2);
        results.add(r3);results.add(r1);
        results.add(r2);
        results.add(r3);



        ResultListAdapter resultListAdapter = new ResultListAdapter(this, R.layout.adapter_view_layout, results);
        listView.setAdapter(resultListAdapter);
    }

    void setLineChartData() {

        ArrayList<Entry> activityValues = new ArrayList<Entry>();
        activityValues.add(new Entry(20f, 14.0F));
        activityValues.add(new Entry(30f, 3.0F));
        activityValues.add(new Entry(40f, 5.0F));
        activityValues.add(new Entry(50f, 1.0F));
        activityValues.add(new Entry(60f, 8.0F));

        ArrayList<Entry> sittingValues = new ArrayList<Entry>();
        sittingValues.add(new Entry(20f, 30F));
        sittingValues.add(new Entry(30f, 6.0F));
        sittingValues.add(new Entry(40f, 8.0F));
        sittingValues.add(new Entry(50f, 42.0F));
        sittingValues.add(new Entry(60f, 31.0F));


        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

        // activity
        LineDataSet activity = new LineDataSet(activityValues, "Activity");
        activity.setColor(Color.GREEN);
        activity.setFillColor(Color.GREEN);
        activity.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        // sitting
        LineDataSet sitting = new LineDataSet(sittingValues, "Sitting");
        sitting.setColor(Color.BLUE);
        sitting.setFillColor(Color.BLUE);

        sitting.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        lineDataSets.add(activity);
        lineDataSets.add(sitting);

        //We connect our data to the UI Screen
        LineData data = new LineData(lineDataSets);
        LineChart lineChart = findViewById(R.id.getTheGraph);
        lineChart.setData(data);
        Description desc = new Description();
        desc.setText("");
        lineChart.setDescription(desc);
        lineChart.setBackgroundColor(Color.WHITE);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getAxisRight().setDrawGridLines(false);
        lineChart.animateXY(500, 500, Easing.EaseInCubic);
    }

}
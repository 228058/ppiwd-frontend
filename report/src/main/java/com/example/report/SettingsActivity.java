package com.example.report;

import android.content.Intent;
import android.graphics.drawable.Icon;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.zip.Inflater;

public class SettingsActivity extends AppCompatActivity {
    private String[] itemsCheckActivityEvery = new String[] {"5 minutes", "15 minutes", "30 minutes"};
    private String[] itemsAlertAfter = new String[] {"2 hours", "4 hours", "8 hours"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Spinners
        Spinner spinnerCheckActivityEvery = findViewById(R.id.spinnerCheckActivityEvery);
        Spinner spinnerAlertAfter = findViewById(R.id.spinnerAlertAfter);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, itemsCheckActivityEvery);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, itemsAlertAfter);
        spinnerCheckActivityEvery.setAdapter(adapter1);
        spinnerAlertAfter.setAdapter(adapter2);

        // Button
        Button saveButton = findViewById(R.id.saveSettingsButton);
        saveButton.setOnClickListener(view -> {
            Toast toast= Toast.makeText(getApplicationContext(),
                    "Settings saved!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.LEFT, 0, 90);
            toast.show();
            this.finish();
        });

        ImageButton imageButton = findViewById(R.id.settingsViewBackImageButton);
        imageButton.setOnClickListener(view -> {
            this.finish();
        });


        // Edit texts
        EditText checkActivityEveryText = findViewById(R.id.editTextTextPersonName);
        checkActivityEveryText.setKeyListener(null);

        EditText alertAfterText = findViewById(R.id.alertAfterEditText);
        alertAfterText.setKeyListener(null);

        EditText pushNotificationText = findViewById(R.id.pushNotificationEditText);
        pushNotificationText.setKeyListener(null);
    }
}
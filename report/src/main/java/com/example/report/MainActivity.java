package com.example.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.widget.Button;

import android.widget.TextView;

import com.mbientlab.bletoolbox.scanner.BleScannerFragment;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.data.Acceleration;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.GyroBmi160;
import com.mbientlab.metawear.module.Logging;

import java.util.UUID;
import java.util.concurrent.CancellationException;

import bolts.Continuation;
import bolts.Task;

public class MainActivity extends AppCompatActivity implements ServiceConnection {

    public final static String EXTRA_BT_DEVICE= "com.mbientlab.metawear.starter.DeviceSetupActivity.EXTRA_BT_DEVICE";
    private static final String KEY_BLUETOOTH_DEVICE = "com.mbientlab.metawear.starter.DeviceSetupActivity.ReconnectDialogFragment.KEY_BLUETOOTH_DEVICE";

    private static final int REQUEST_BT_DEVICE = 1;


    private BluetoothDevice btDevice = null;
    private BtleService.LocalBinder serviceBinder;
    private MetaWearBoard metawear;

    Button checkMyReportButton;
    Button connectDeviceButton;
    TextView deviceIdText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        Log.i("report", "onCreate()");

        getApplicationContext().bindService(new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);

        checkMyReportButton = findViewById(R.id.checkMyReport);
        connectDeviceButton = findViewById(R.id.connectDevice);
        deviceIdText = findViewById(R.id.deviceIdText);

        checkMyReportButton.setOnClickListener(view -> {
            Intent myIntent = new Intent(this, ListActivity.class);
            this.startActivity(myIntent);
        });

        connectDeviceButton.setOnClickListener(view -> {
            Intent myIntent = new Intent(this, ChooseDeviceActivity.class);
            this.startActivityForResult(myIntent, REQUEST_BT_DEVICE);
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ///< Unbind the service when the activity is destroyed
        getApplicationContext().unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.i("report", "onServiceConnected called");
        serviceBinder = (BtleService.LocalBinder) service;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_BT_DEVICE) {
            if(resultCode == Activity.RESULT_OK){
                Log.i("report", "device retrieve succeeded");
                String MW_MAC_ADDRESS = data.getStringExtra("result");

                final BluetoothManager btManager=
                        (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                final BluetoothDevice remoteDevice=
                        btManager.getAdapter().getRemoteDevice(MW_MAC_ADDRESS);

                metawear = serviceBinder.getMetaWearBoard(remoteDevice);

                deviceIdText.setText(metawear.getModelString());


                Thread thread = new Thread(){

                    Logging logging;

                    boolean threadRunning = true;
                    int sampleCounter = 0;
                    int loopCounter = 0;

                    Accelerometer accelerometer;
                    GyroBmi160 gyro;

                    boolean initialRun = true;

                    public void run() {

                        System.out.println("Thread Running");
                        Log.i("report", "metawear: " + metawear.getModelString());
                        while (loopCounter < 5) {

                            Log.i("report", " run: " + loopCounter);

                            Task metawearTask = metawear.connectAsync().onSuccessTask(task -> {

                                return null;
                            }).continueWith(task -> {
                                if (task.isCompleted()) {
                                    Log.i("report", " Connected");
                                    try {
                                        accelerometer = metawear.getModule(Accelerometer.class);
                                        accelerometer.configure()
                                                .odr(100f)      // Set sampling frequency to 100Hz, or closest valid ODR
                                                .range(4f)      // Set data range to +/-4g, or closet valid range
                                                .commit();


                                        gyro = metawear.getModule(GyroBmi160.class);
                                        gyro.configure()
                                                .odr(GyroBmi160.OutputDataRate.ODR_100_HZ)
                                                .range(GyroBmi160.Range.FSR_2000)
                                                .commit();

                                        if (initialRun == true) {
                                            Log.i("report", " initial run");
                                            Task accTask = accelerometer.acceleration().addRouteAsync(source -> {
                                                source.log(
                                                        (data, env) -> {
                                                            Log.i("report", data.toString());
                                                        }
                                                );
                                            });
                                            accTask.waitForCompletion();
                                            boolean accTaskSuccess = accTask.isCompleted();
                                            if (accTaskSuccess) {
                                                Log.i("report", " acc task completed");
                                            } else if (accTask.isFaulted()) {
                                                Log.i("report", " acc task failed");
                                                throw accTask.getError();
                                            } else if (accTask.isCancelled()) {
                                                Log.i("report", " acc task cancelled");
                                                throw new CancellationException("Task cancelled");
                                            } else {
                                                Log.i("report", " acc task not completed");
                                                throw new Exception("Task cancelled");
                                            }
                                            accelerometer.acceleration().start();
                                            accelerometer.start();

                                            Logging logging = metawear.getModule(Logging.class);
                                            logging.start(false);

                                            initialRun = false;
                                        }

                                        Thread.sleep(5000);

                                        if (loopCounter == 4)
                                        {
                                            Log.i("report", "last loop");
                                            accelerometer.acceleration().stop();
                                            accelerometer.stop();

                                            logging = metawear.getModule(Logging.class);
                                            logging.stop();

                                            // download log data and send 100 progress updates during the download
                                            Task downloadTask = logging.downloadAsync(100, new Logging.LogDownloadUpdateHandler() {
                                                @Override
                                                public void receivedUpdate(long nEntriesLeft, long totalEntries) {
                                                    Log.i("report", "Progress Update = " + nEntriesLeft + "/" + totalEntries);
                                                }
                                            }).continueWithTask(new Continuation<Void, Task<Void>>() {
                                                @Override
                                                public Task<Void> then(Task<Void> task) throws Exception {
                                                    Log.i("report", "Download completed");
                                                    return null;
                                                }
                                            });

                                            downloadTask.waitForCompletion();
                                            boolean accTaskSuccess = downloadTask.isCompleted();
                                            if (accTaskSuccess) {
                                                Log.i("report", " downloadTask completed");
                                            } else if (downloadTask.isFaulted()) {
                                                Log.i("report", " downloadTask failed");
                                                throw downloadTask.getError();
                                            } else if (downloadTask.isCancelled()) {
                                                Log.i("report", " downloadTask cancelled");
                                                throw new CancellationException("downloadTask cancelled");
                                            } else {
                                                Log.i("report", " downloadTask not completed");
                                                throw new Exception("downloadTask cancelled");
                                            }
                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    //                                    fuseImuData();
                                } else if (task.isFaulted()) {
                                    Log.i("report", " Failed to connect");
                                } else if (task.isCancelled()) {
                                    Log.i("report", " Connection task cancelled");
                                }
                                return null;
                            });


                            try {
                                metawearTask.waitForCompletion();
                                boolean gyroTaskSuccess = metawearTask.isCompleted();
                                if (gyroTaskSuccess) {
                                    Log.i("report", " metawearTask completed");
                                } else if (metawearTask.isFaulted()) {
                                    Log.i("report", " metawearTask failed");
                                    throw metawearTask.getError();
                                } else if (metawearTask.isCancelled()) {
                                    Log.i("report", " metawearTask cancelled");
                                    throw new CancellationException("metawearTask cancelled");
                                } else {
                                    Log.i("report", " metawearTask not completed");
                                    throw new Exception("metawearTask cancelled");
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            metawear.disconnectAsync();

                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                Log.i("report", "sleep exception");
                                e.printStackTrace();
                            }

                            loopCounter++;
                        }

                        Log.i("report", "Thread stopping");
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };

                thread.start();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("report", "device retrieve failed");
            }
        }
    } //onActivityResult
}
